BASE=project
LATEX=latex
BIBTEX=bibtex

all:$(BASE).pdf

%.pdf:	%.tex
	pdflatex --shell-escape $(BASE).tex
	bibtex $(BASE).aux
	pdflatex --shell-escape $(BASE).tex
	pdflatex --shell-escape $(BASE).tex

clean:
	rm -f $(BASE).aux $(BASE).pdf $(BASE).log $(BASE).blg $(BASE).bbl $(BASE).synctex.gz $(BASE).toc
